import sys
import click


def my_func():
    args = sys.argv
    print("args:", args)


@click.command()
@click.argument(
    "number",
    type=click.INT,
    default=1,
)
@click.option(
    "--name",
    type=click.STRING,
    default="John",
    help="Your name",
)
def my_cool_func(number, name):
    click.echo(f"number: {number}, my_name: {name}")


if __name__ == "__main__":
    my_cool_func()
